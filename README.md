# Stonetoss

This repo is dedicated to archiving the files of <a href="https://stonetossisanazi.org">stonetossisanazi.org</a>

This repo is licensed under the Apache 2.0 license.

I encourage you to mirror these files in your own web server.

If you choose to mirror these files, check out <a href="https://codeberg.org/firebadnofire/git-sync">git-sync</a> to keep this up-to-date with this repo automatically. 

If you choose to add files to <a href="https://codeberg.org/firebadnofire/stonetoss/src/branch/main/archive/archive/">/archive/archive/</a> please fork the repo and make a pull request.


